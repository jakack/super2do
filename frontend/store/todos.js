export const state = () => ({
  todoList: [],
})

export const getters = {
  all(state) {
    return state.todoList
  },
  progress(state) {
    return state.todoList.filter(function (item) {
      return !item.isComplete
    })
  },
  done(state) {
    return state.todoList.filter(function (item) {
      return item.isComplete
    })
  },
}

export const mutations = {
  store(state, data) {
    state.todoList = data
  },
  add(state, content) {
    state.todoList.push(content)
  },
  remove(state, todo) {
    const index = state.todoList.findIndex(
      (todoItem) => todoItem.id === todo.id
    )
    state.todoList.splice(index, 1)
  },
  toggle(state, todo) {
    const index = state.todoList.findIndex(
      (todoItem) => todoItem.id === todo.id
    )
    state.todoList[index].isComplete = todo.isComplete
  },
  edit(state, todo) {
    const index = state.todoList.findIndex(
      (todoItem) => todoItem.id === todo.id
    )
    state.todoList[index].content = todo.content
  },
}

export const actions = {
  getTodoList(vuexContext) {
    return this.$axios
      .$get('http://localhost:5000/api/todo')
      .then((res) => {
        vuexContext.commit('store', res)
      })
  },
  addTodo(vuexContext, content) {
    return this.$axios
      .$post('http://localhost:5000/api/todo', {
        content,
        isComplete: false,
      })
      .then(function (res) {
        vuexContext.commit('add', res)
        console.log(res)
      })
      .catch(function (err) {
        alert(err)
      })
  },
  editTodo(vuexContext, data) {
    return this.$axios
      .$put(
        `http://localhost:5000/api/todo/${data.todo.id}`,
        {
          content: data.content,
        }
      )
      .then(function (res) {
        var params = {
          id: res.id,
          content: data.content,
          isComplete: res.isComplete,
        };
        vuexContext.commit('edit', params)
      })
      .catch(function (err) {
        alert(err)
      })
  },
  toggleTodo(vuexContext, todo) {
    return this.$axios
      .$put(`http://localhost:5000/api/todo/${todo.id}`, {
        isComplete: !todo.isComplete,
      })
      .then(function (res) {
        vuexContext.commit('toggle', res)
      })
      .catch(function (err) {
        alert(err)
      })
  },
  completedTodo(vuexContext) {
    return this.$axios
    .$get('http://localhost:5000/api/todo/completed')
    .then((res) => {
      vuexContext.commit('store', res)
    })
  },
  deleteTodo(vuexContext, todo) {
    return this.$axios
      .$delete(`http://localhost:5000/api/todo/${todo.id}`)
      .then(function (res) {
        vuexContext.commit('remove', res)
      })
      .catch(function (err) {
        alert(err)
      })
  },
}
