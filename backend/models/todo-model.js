const koneksi = require('../config/database');
const { responseData, responseMessage } = require('../utils/response-handler');
const ErrorResponse = require('../utils/errorResponse');

// insert todo
exports.insertTodo = (response, statement, data, next) => {
    // jalankan query
    koneksi.query(statement, data, (err, rows, field) => {
        // error handling
        if (err) {
            return next(new ErrorResponse(err.message, 500));
        }

        koneksi.query("SELECT * FROM todo WHERE id = ?", rows.insertId, (err, rows, field) => {
            // error handling
            if (err) {
                return next(new ErrorResponse(err.message, 500));
            }
    
            // jika request berhasil
            responseData(response, 200, rows[0]);
        });

        // // jika request berhasil
        // // responseMessage(response, 201, 'Berhasil insert data!');
        // responseMessage(response, 201, 'Berhasil insert data!', rows);
    });
};

// get data Todo
exports.getTodos = (response, statement, next) => {
    // jalankan query
    koneksi.query(statement, (err, rows, field) => {
        // error handling
        if (err) {
            return next(new ErrorResponse(err.message, 500));
        }

        // jika request berhasil
        responseData(response, 200, rows);
    });
};

// get data Todo
exports.getTodoById = (response, statement, id, next) => {
    // jalankan query
    koneksi.query(statement, id, (err, rows, field) => {
        // error handling
        if (err) {
            return next(new ErrorResponse(err.message, 500));
        }

        // jika request berhasil
        responseData(response, 200, rows[0]);
    });
};


// update data Todo
exports.updateTodo = (response, searchStatement, updateStatement, id, data, next) => {
    // jalankan query untuk melakukan pencarian data
    koneksi.query(searchStatement, id, (err, rows, field) => {
        // error handling
        if (err) {
            return next(new ErrorResponse(err.message, 500));
        }
        result = rows[0];
        // jika id yang dimasukkan sesuai dengan data yang ada di db
        if (rows.length) {
            // jalankan query update
            koneksi.query(updateStatement, [data, id], (err, rows, field) => {
                // error handling
                if (err) {
                    return next(new ErrorResponse(err.message, 500));
                }

                // jika update berhasil
                responseMessage(response, 200, 'Berhasil update data!', result);
            });
        } else {
            return next(new ErrorResponse('Data tidak ditemukan!', 500));
        }
    });
};

exports.updateTodoToCompleted = (response, updateStatement, searchStatement, next) => {
    // jalankan query untuk melakukan pencarian data
    //
    koneksi.query(updateStatement, (err, rows, field) => {
        // error handling
        if (err) {
            return next(new ErrorResponse(err.message, 500));
        }

        koneksi.query(searchStatement, (err, rows, field) => {
            // error handling
            if (err) {
                return next(new ErrorResponse(err.message, 500));
            }
    
            // jika request berhasil
            responseData(response, 200, rows);
        });
        
    });
};

// delete Todo
exports.deleteTodo = (response, searchStatement, deleteStatement, id, next) => {
    // jalankan query untuk melakukan pencarian data
    koneksi.query(searchStatement, id, (err, rows, field) => {
        // error handling
        if (err) {
            return next(new ErrorResponse(err.message, 500));
        }

        // jika id yang dimasukkan sesuai dengan data yang ada di db
        if (rows.length) {
            var values
            koneksi.query("SELECT * FROM todo WHERE id = ?", id, (err, rows, field) => {
                // error handling
                if (err) {
                    return next(new ErrorResponse(err.message, 500));
                }
                values = rows[0];
            });

            
            // jalankan query delete
            koneksi.query(deleteStatement, id, (err, rows, field) => {
                // error handling
                if (err) {
                    return next(new ErrorResponse(err.message, 500));
                }

                // jika delete berhasil
                responseMessage(response, 200, 'Berhasil hapus data!', values);
            });
        } else {
            return next(new ErrorResponse('Data tidak ditemukan!', 500));
        }
    });
};

