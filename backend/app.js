const express = require('express');
const bodyParser = require('body-parser');
const todoRouter = require('./routes/todo--router');
const errorHandler = require('./middleware/error');
const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 5000;


app.use(cors());


// set body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// set routing
app.use('/api/todo', todoRouter);

// set error middleware
app.use(errorHandler);

// buat server nya
app.listen(PORT, () => console.log(`Server running at port: ${PORT}`));