const { 
    createData, 
    readData,
    readDataById,
    updateData,
    updateDataCompleted,
    deleteData 
} = require('../controllers/todo-controller');
const express = require('express');
const router = express.Router();


router.route('/completed')
    .get(updateDataCompleted);

router.route('/')
    .post(createData)
    .get(readData);

router.route('/:id')
    .get(readDataById)
    .put(updateData)
    .delete(deleteData);

module.exports = router;