const validate = require('validate.js');

exports.validateTodo = (data) => {
    // user schema
    var constraint = {
        content: {
            presence: {
                allowEmpty: false
            }
        },
    };

    return validate(data, constraint, { format: 'flat' });
};