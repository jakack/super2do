
const responseData = function (response, statusCode, values) {
    var data = {
        success: true,
        data: values,
    };
    response.status(statusCode).json(values);
    response.end();
};

const responseMessage = function (response, statusCode, message, values) {
    var data = {
        success: true,
        message: message,
        values: values,
    };
    response.status(statusCode).json(values);
    response.end();
};

module.exports = { responseData, responseMessage };