const { 
    insertTodo, 
    getTodos,
    getTodoById,
    updateTodo,
    updateTodoToCompleted,
    deleteTodo 
} = require('../models/todo-model');
const { validateTodo } = require('../utils/validation');
const ErrorResponse = require('../utils/errorResponse');

// create todo
exports.createData = (req, res, next) => {
    // buat variabel penampung data dan query sql
    const data = { ...req.body };
    const querySql = 'INSERT INTO todo SET ?';

    // validasi
    var errors = validateTodo(data);
    if (errors) {
        return next(new ErrorResponse(errors[0], 400));
    }

    // masukkan ke dalam model
    insertTodo(res, querySql, data, next);
};

// show todo
exports.readData = (req, res, next) => {
    // buat query sql
    const querySql = 'SELECT * FROM todo';

    // masukkan ke dalam model
    getTodos(res, querySql, next);
};


// show todo by id
exports.readDataById = (req, res, next) => {
    // buat query sql
    const querySql = 'SELECT * FROM todo WHERE id = ?';

    // masukkan ke dalam model
    getTodoById(res, querySql, req.params.id, next);
};

// update todo
exports.updateData = (req, res, next) => {
    // buat variabel penampung data dan query sql
    const data = { ...req.body };
    const querySearch = 'SELECT * FROM todo WHERE id = ?';
    const queryUpdate = 'UPDATE todo SET ? WHERE id = ?';

    // masukkan ke dalam model
    updateTodo(res, querySearch, queryUpdate, req.params.id, data, next);
};

// update todo to completed
exports.updateDataCompleted = (req, res, next) => {
    // buat variabel penampung data dan query sql
    const data = { ...req.body };
    const querySelect = 'SELECT * FROM todo';
    const queryUpdate = 'UPDATE todo SET isComplete = 1';

    // masukkan ke dalam model
    updateTodoToCompleted(res, queryUpdate, querySelect, next);
};

// delete todo
exports.deleteData = (req, res, next) => {
    // buat query sql untuk mencari data dan hapus
    const querySearch = 'SELECT * FROM todo WHERE id = ?';
    const queryDelete = 'DELETE FROM todo WHERE id = ?';

    // masukkan ke dalam model
    deleteTodo(res, querySearch, queryDelete, req.params.id, next);
};